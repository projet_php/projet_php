<?php
require 'sourcefiles/libs/app.php';
require 'sourcefiles/libs/controller.php';
require 'sourcefiles/libs/view.php';
require 'sourcefiles/libs/Model.php';
require 'sourcefiles/config/paths.php';
require 'sourcefiles/libs/database.php';
require 'sourcefiles/libs/session.php';
require 'sourcefiles/libs/traduction.php';
require 'sourcefiles/libs/export_gettext.php';
$app = new app();
?>