create database if not exists libretrad;
use libretrad;

create table if not exists utilisateurs
(id_utilisateur int unsigned auto_increment,
pseudonyme varchar(128) not null unique,
adresse_electronique varchar(128) not null,
mot_de_passe varchar(128) not null,
type_de_compte varchar(16) not null,
verifie boolean not null default 0,
cle varchar(32) not null,
primary key (id_utilisateur))
default character set utf8mb4 collate utf8mb4_unicode_ci;

create table if not exists langues
(id_langue int unsigned auto_increment,
nom varchar(128) not null unique,
primary key (id_langue))
default character set utf8mb4 collate utf8mb4_unicode_ci;

create table if not exists traductions_demandees
(id_demande_traduction int unsigned auto_increment,
langue_source int unsigned not null,
index index_langue_source (langue_source),
phrase_a_traduire text not null,
identifiant_requerant int unsigned not null,
index index_identifiant_requerant (identifiant_requerant),
foreign key (langue_source) references langues(id_langue),
foreign key (identifiant_requerant)
  references utilisateurs(id_utilisateur)
  on delete cascade,
primary key (id_demande_traduction))
default character set utf8mb4 collate utf8mb4_unicode_ci;

create table if not exists traductions
(id_traduction int unsigned auto_increment,
langue_destination int unsigned not null,
phrase_a_traduire int unsigned not null,
index index_phrase_a_traduire (phrase_a_traduire),
phrase_traduite text,
identifiant_traducteur int unsigned,
foreign key (langue_destination)
  references langues(id_langue),
foreign key (phrase_a_traduire)
references traductions_demandees(id_demande_traduction),
primary key (id_traduction))
default character set utf8mb4 collate utf8mb4_unicode_ci;
