<?php
class Header_Model extends Model
{
  /* Permet d’obtenir une liste des langues. */
  function listeLangue()
  {
    $reqLangue = $this->db->prepare("SELECT * FROM langues");
    $reqLangue->execute();
    $data = $reqLangue->fetchAll();
    return $data;
  }
}
?>
