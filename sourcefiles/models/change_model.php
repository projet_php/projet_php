<?php
class Change_Model extends Model{
  /* Permet de modifier les informations de l’utilisateur. */
  function update($pseudo,$mail){
    Session::init();
    $statement =  $this->db->prepare('UPDATE utilisateurs SET adresse_electronique = :email, pseudonyme = :pseudo WHERE id_utilisateur = :id_util' );
    if($statement->execute(array(
                                 ':email' => $mail,
                                 ':pseudo' => $pseudo,
                                 ':id_util' => Session::get('id_util')
                                 )))
      {
        Session::set('pseudo',$pseudo);
        Session::set('mail',$mail);
        return true;
      }
    else
      {
        echo "non";
      }
  }
}
?>
