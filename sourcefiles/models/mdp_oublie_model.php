<?php
class MDP_Oublie_Model extends Model
{
  /* Permet de vérifier si la clef correspond bien à l’utilisateur. */
  public function vérifier($pseudo, $clef)
  {
    $requête = $this->db->prepare('select * from utilisateurs where cle = :clef and pseudonyme = :pseudo');

    if($requête->execute(array(
                               "pseudo" => $pseudo,
                               "clef" => $clef
                               )))
      {
        return true;
      }
    else
      {
        return false;
      }
  }

  /* Permet de changer le mot de passe d’un utilisateur. */
  public function changer ($pseudo, $mdp)
  {
    $requête = $this->db->prepare ('update utilisateurs set mot_de_passe = :mdp '
                                   . 'where pseudonyme = :pseudo');
    return $requête->execute (array ("mdp" => password_hash ($mdp, PASSWORD_BCRYPT),
                                     "pseudo" => $pseudo));
  }

  /* Permet d’obtenir l’adresse électronique d’un utilisateur. */
  public function obtenir_adresse ($pseudo)
  {
    $requête = $this->db->prepare ('select adresse_electronique from utilisateurs '
                                   . 'where pseudonyme = :pseudo');
    $requête->execute (array ('pseudo' => $pseudo));
    return $requête->fetch ()['adresse_electronique'];
  }

  /* Permet de changer la clef d’un utilisateur. */
  public function définir_clef ($pseudo, $clef)
  {
    $requête = $this->db->prepare ('update utilisateurs set cle = :clef '
                                   . 'where pseudonyme = :pseudo');
    return $requête->execute (array ('clef' => $clef, 'pseudo' => $pseudo));
  }
}
?>
