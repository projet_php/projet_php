<?php
class ChangerUtilisateurs_Model extends Model
{
  /* Permet de changer le type de compte d’un utilisateur. */
  public function update_utilisateurs($compte,$pseudo)
  {
    $reqUtilisateurs = $this->db->prepare('UPDATE utilisateurs SET type_de_compte = :compte WHERE pseudonyme = :pseudo');

    if($reqUtilisateurs->execute(array(
                                       ":compte" => $compte,
                                       ":pseudo" => $pseudo)))
      {
        return true;
      } else {
      var_dump($reqUtilisateurs->errorInfo(),$reqUtilisateurs->debugDumpParams());
      die;
    }
  }
}
?>
