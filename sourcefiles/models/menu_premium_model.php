<?php
class Menu_Premium_Model extends Model
{
  /* Permet d’obtenir le nombre des demandes de traduction d’un
     certain utilisateur.  */
  public function get_occurence()
  {
   $reqUtilisateurs = $this->db->prepare('SELECT COUNT(*) FROM traductions WHERE identifiant_requerant = :id');
         if($reqUtilisateurs->execute(
          array("id" => $id)
          )){
          return $reqUtilisateurs->fetchAll();
         } else {
          var_dump($reqUtilisateurs->errorInfo(),$reqUtilisateurs->debugDumpParams());
          die;
         } 
  }

  /* Permet d’obtenir une liste des demandes de traductions d’un certain utilisateur. */
  public function afficher_demandes($id)
  {
        $reqUtilisateurs = $this->db->prepare('SELECT * FROM traductions_demandees WHERE identifiant_requerant = :id');
         if($reqUtilisateurs->execute(
          array("id" => $id)
          )){
          return $reqUtilisateurs->fetchAll();
         } else {
          var_dump($reqUtilisateurs->errorInfo(),$reqUtilisateurs->debugDumpParams());
          die;
         }
  }
}
?>
