<?php
class Ask_Trad_Model extends Model
{
  /* Permet d’obtenir l’identifiant d’une langue depuis son nom. */
  function obtenir_id_langue ($langue)
  {
    $requête_id_langue_source = $this->db->prepare ("select id_langue from langues where nom = :langue_source");
    $requête_id_langue_source->execute (array ("langue_source" => $langue));
    $id_langue_source = $requête_id_langue_source->fetch (PDO::FETCH_ASSOC);
    return (int) $id_langue_source["id_langue"];
  }

  /* Permet d’insérer une demande de traduction dans la base de
     données. */
  public function ask_for_trad($langueDep,$traductionDemandee)
  {
    $requête = $this->db->prepare('insert into traductions_demandees (langue_source, phrase_a_traduire, identifiant_requerant) values (:langue, :phrase, :id)');

    $langue = $this->obtenir_id_langue($langueDep);

    if($requête->execute(array(
                               "langue" => $langue,
                               "phrase"  => $traductionDemandee,
                               "id"  => Session::get('id_util')
                               ))) {
      return true;
    } else {
      var_dump($requête->errorInfo(),$requête->debugDumpParams());
      die;
    }
  }
}
?>
