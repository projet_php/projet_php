<?php
class Menu_Admin_Model extends Model
{
  /* Renvoit une liste des utilisateurs. */
  public function afficher_Utilisateurs()
  {
        $reqUtilisateurs = $this->db->prepare('SELECT * FROM utilisateurs');

         if($reqUtilisateurs->execute()){
          return $reqUtilisateurs->fetchAll();
         } else {
          var_dump($reqUtilisateurs->errorInfo(),$reqUtilisateurs->debugDumpParams());
          die;
         }
  }

  /* Renvoit une liste des demandes de traductions. */
  public function afficher_Demandes()
  {
        $reqDemandeTrad = $this->db->prepare('SELECT * FROM traductions_demandees,utilisateurs WHERE identifiant_requerant = id_utilisateur');

         if($reqDemandeTrad->execute()){
          return $reqDemandeTrad->fetchAll();
         } else {
          var_dump($reqDemandeTrad->errorInfo(),$reqDemandeTrad->debugDumpParams());
          die;
         }
  }
}
?>
