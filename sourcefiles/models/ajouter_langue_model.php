<?php
class Ajouter_langue_Model extends Model{
  /* Permet d’ajouter une langue dans la table des langues. */
  function Ajouter_langue($nom){
    Session::init();
    $statement =  $this->db->prepare('INSERT INTO langues (nom) VALUES (:nom)');
    if($statement->execute(array(
                                 'nom' => $nom
                                 )))
      return true;
  }
}
?>
