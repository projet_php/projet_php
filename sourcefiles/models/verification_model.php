<?php
class verification_Model extends Model
{
  /* Renvoit si l’utilisateur a un compte vérifié et sa clef. */
  public function verify($pseudo)
  {
    $requête = $this->db->prepare('SELECT cle,verifie FROM utilisateurs WHERE pseudonyme =:pseudo');

    if($requête->execute(array(
                               "pseudo" => $pseudo
                               ))) {
      $data = $requête->fetch();
      return $data;
    } else {
      var_dump($requête->errorInfo(),$requête->debugDumpParams());
      die;
    }
  }

  /* Valide l’adresse électronique du compte. */
  public function update($pseudo, $clef)
  {
    $reqUpdateVerifie = $this->db->prepare("UPDATE utilisateurs SET verifie = 1, cle = :cle WHERE pseudonyme = :pseudo ");
    if($reqUpdateVerifie->execute(array(
                                        "pseudo" => $pseudo,
                                        "cle" => $clef
                                        )))
      {
        return true;
      }
  }
}
?>
