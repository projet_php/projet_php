<?php

class Traduire_Model extends Model
{
  /* Permet d’obtenir l’identifiant d’une langue depuis son nom. */
  function obtenir_id_langue ($langue)
  {
    $requête_id_langue_source = $this->db->prepare ("select id_langue from langues where nom = :langue_destination");
    $requête_id_langue_source->execute (array ("langue_destination" => $langue));
    $id_langue_source = $requête_id_langue_source->fetch (PDO::FETCH_ASSOC);
    return (int) $id_langue_source["id_langue"];
  }

  /* Permet d’insérer une traduction dans la base de données. */
  public function insert_trad($langue,$traduction)
  {
    $requête = $this->db->prepare('insert into traductions (phrase_traduite, identifiant_traducteur, phrase_a_traduire,langue_destination) values (:traducion, :ident, :demande, :destination)');

    $langue_destination = $this->obtenir_id_langue($langue);

    if($requête->execute(array(
                               ":traducion" => $traduction,
                               ":ident" => Session::get('id_util'),
                               ":demande" => Session::get('id_article'),
                               ":destination" => $langue_destination
                               ))) {
      return true;
    } else {
      var_dump($requête->errorInfo(),$requête->debugDumpParams());
      die;
    }
  }
}
?>
