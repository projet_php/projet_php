<?php
class Recherche_Model extends Model
{
  /* Permet d’obtenir l’identifiant d’une langue depuis son nom. */
  function obtenir_id_langue ($langue)
  {
    $requête_id_langue_source = $this->db->prepare ("select id_langue from langues where nom = :langue_source");
    $requête_id_langue_source->execute (array ("langue_source" => $langue));
    $id_langue_source = $requête_id_langue_source->fetch (PDO::FETCH_ASSOC);
    return (int) $id_langue_source["id_langue"];
  }

  /* Permet d’obtenir la traduction d’une phrase. */
  function recherche($texte,$langueDep,$langueFin)
  {
    $id_langue_source = $this->obtenir_id_langue ($langueDep);
    $id_langue_destination = $this->obtenir_id_langue ($langueFin);

    $reqRecherche = $this->db->prepare("select phrase_traduite from traductions, traductions_demandees where traductions.phrase_a_traduire = id_demande_traduction and traductions_demandees.phrase_a_traduire = :phrase_a_traduire and langue_source = :langue_source and langue_destination = :langue_destination;");

    $tableau = array ("phrase_a_traduire" => $texte,
                      "langue_source" => $id_langue_source,
                      "langue_destination" => $id_langue_destination);
    $reqRecherche->execute($tableau);

    return $reqRecherche->fetch(PDO::FETCH_ASSOC)["phrase_traduite"];
  }
}
?>
