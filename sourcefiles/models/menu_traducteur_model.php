<?php
class Menu_Traducteur_Model extends Model
{
  /* Permet d’obtenir une liste des demandes de traduction d’un
     certain utilisateur. */
  public function afficher_Demandes()
  {
    $reqDemandeTrad = $this->db->prepare('SELECT * FROM traductions_demandees,utilisateurs WHERE identifiant_requerant = id_utilisateur');

    if($reqDemandeTrad->execute()){
      return $reqDemandeTrad->fetchAll();
    } else {
      var_dump($reqDemandeTrad->errorInfo(),$reqDemandeTrad->debugDumpParams());
      die;
    }
  }
}
?>
