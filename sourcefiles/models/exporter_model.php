<?php
class Exporter_Model extends Model
{
  /* Permet d’obtenir une liste des demandes de traduction d’un
     utilisateur donné. */
  public function obtenir_demandes ($id)
  {
    $requête = $this->db->prepare ('select phrase_a_traduire from traductions_demandees where identifiant_requerant = :id');
    if ($requête->execute (array ("id" => $id)))
      {
        return $requête->fetchAll ();
      }
    else
      die;
  }
}
?>
