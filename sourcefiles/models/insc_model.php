<?php
class Insc_Model extends Model
{
  /* Permet d’ajouter un utilisateur dans la base de données. */
  public function verify($pseudo,$mot_de_passe,$adresse_électronique,$compte,$clé)
  {
    $requête = $this->db->prepare('insert into utilisateurs (pseudonyme, adresse_electronique, mot_de_passe, type_de_compte, cle) values (:pseudo, :courriel, :mdp, :compte, :cle)');

    if($requête->execute(array(
                               "pseudo" => $pseudo,
                               "courriel"  => $adresse_électronique,
                               "mdp"    => password_hash($mot_de_passe,PASSWORD_BCRYPT),
                               "compte" => $compte,
                               "cle" => $clé
                               ))) {
      return true;
    } else {
      var_dump($requête->errorInfo(),$requête->debugDumpParams());
      die;
    }
  }
}
?>
