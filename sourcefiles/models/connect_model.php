<?php

class Connect_Model extends Model
{
  /* Permet de vérifier si le mot de passe est correcte. */
  public function verify()
  {
    $statement =  $this->db->prepare('SELECT * FROM utilisateurs WHERE pseudonyme = :pseudo');
    $statement->execute(array(
                              ':pseudo' => $_POST['pseudo']
                              ));

    $data = $statement->fetch();
    $count = $statement->rowCount();

    if($count > 0 && password_verify ($_POST['password'], $data['mot_de_passe']))
      {
        if($data['verifie'] == '1')
          {
            Session::init();
            Session::set('loggedIn',true);
            Session::set('Compte',$data['type_de_compte']);
            Session::set('pseudo',$data['pseudonyme']);
            Session::set('mail',$data['adresse_electronique']);
            Session::set('id_util',$data['id_utilisateur']);

            header('Location: ../menu_'.$data['type_de_compte']);

          }
        else
          {
            header("location: ../compte_non_valide");
          }
      }
    else
      {
        header("location: ../connect");
      }
  }
}

?>
