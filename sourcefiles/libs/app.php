<?php
class app
{
  function __construct() {

    $url = isset($_GET['url']) ? $_GET['url'] : null;
    $url = rtrim($url, '/');
    $url = explode('/', $url);

    /* Si l’url est vide, on appelle le contrôleur index. */
    if (empty($url[0])) {
      require 'sourcefiles/controllers/index.php';
      $controller = new Index();
      $controller->index();
      return false;
    }

    /* Sinon, on vérifie que c’est bien l’adresse d’un contrôleur. */
    $file = 'sourcefiles/controllers/' . $url[0] . '.php';
    if (file_exists($file)) {
      require $file;
    } else {
      require 'sourcefiles/controllers/erreur.php';
      $controller = new Erreur();
      $controller->index();
      Session::init();
      return false;
    }

    $controller = new $url[0];
    $controller->loadModel($url[0]);

    // Appel des méthodes
    if (isset($url[2])) {
      if (method_exists($controller, $url[1])) {
        $controller->{$url[1]}($url[2]);
      } else {
        $this->error();
      }
    } else {
      if (isset($url[1])) {
        if (method_exists($controller, $url[1])) {
          $controller->{$url[1]}();
        } else {
          $this->error();
        }
      } else {
        $controller->index();
      }
    }
  }

  function error() {
    require 'sourcefiles/controllers/error.php';
    $controller = new NotFound();
    $controller->index();
    return false;
  }
}
?>
