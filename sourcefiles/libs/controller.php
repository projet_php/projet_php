<?php
class Controller
{
  function __construct()
  {
    $this->view = new View();
  }

  /* Charge le modèle */
  public function loadModel($name)
  {
    $path = 'sourcefiles/models/'.$name.'_model.php';

    if(file_exists($path))
      {
        require 'sourcefiles/models/'.$name.'_model.php';
        $modelName = $name.'_Model';
        $this->model = new $modelName();
      }
  }
}
?>
