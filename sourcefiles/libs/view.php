<?php

class view
{
  function __construct()
  {
    Session::init();
  }

  public function render($nom, $param = false)
  {
    require 'sourcefiles/views/header.php';
    require 'sourcefiles/views/' . $nom . '.php';
    require 'sourcefiles/views/footer.php';
    require 'sourcefiles/models/header_model.php';
    $model_init_languages = new Header_Model();
    $data = $model_init_languages->listeLangue();
    Session::set('lang',$data);
  }
}
?>
