<?php

class Traduction
{
  /* Renvoit l’identifiant d’une langue par son nom. */
  private static function obtenir_id_langue ($langue)
  {
    $bd = new Database ();

    $requête_id_langue_source = $bd->prepare ("select id_langue from langues where nom = :langue_source");
    $requête_id_langue_source->execute (array ("langue_source" => $langue));
    $id_langue_source = $requête_id_langue_source->fetch (PDO::FETCH_ASSOC);
    return (int) $id_langue_source["id_langue"];
  }

  /* Renvoit la traduction d’un texte dans une langue donnée. */
  public static function traduire ($texte, $langue = null)
  {
    /* Si la langue n’est pas spécifiée, on prend celle de la session. */
    if (!$langue)
      {
        $langue = Session::get ('langue');
        if (!$langue)
          return $texte;
      }

    $bd = new Database ();
    $langue = Traduction::obtenir_id_langue ($langue);

    /* Chercher le texte dans la base de données */
    $requête = 'select * from traductions_demandees where ';
    $requête .= 'phrase_a_traduire = :texte and langue_source = 1';

    $requête = $bd->prepare ($requête);
    if (!$requête->execute (array ("texte" => $texte)))
      return $texte;

    $résultat = $requête->fetch (PDO::FETCH_ASSOC);

    /* Le texte n’a pas de demande de traduction. */
    if (!$résultat)
      return $texte;

    $id_demande_traduction = (int) $résultat["id_demande_traduction"];

    $requête = 'select phrase_traduite from traductions where ';
    $requête .= 'phrase_a_traduire = :id_demande_traduction';
    $requête .= ' and langue_destination = :langue_destination';

    $requête = $bd->prepare ($requête);

    $tableau = array ("id_demande_traduction" => $id_demande_traduction,
                      "langue_destination" => $langue);

    if (!$requête->execute ($tableau))
      return $texte;
    $résultat = $requête->fetch (PDO::FETCH_ASSOC);
    if (!$résultat)
      return $texte;
    else
      return filter_var ($résultat["phrase_traduite"], FILTER_SANITIZE_STRING);
  }
}
?>
