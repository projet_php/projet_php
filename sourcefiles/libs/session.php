<?php
class Session
{
  /* Initialise la session. */
  public static function init()
  {
    @session_start();
  }

  /* Définit une entrée dans la session. */
  public static function set($key,$value)
  {
    $_SESSION[$key] = $value;
  }

  /* Renvoit une entrée de la session. */
  public static function get($key)
  {
    if(isset($_SESSION[$key]))
      return $_SESSION[$key];
  }

  /* Termine la session. */
  public static function destroy()
  {
    session_destroy();
  }
}
?>
