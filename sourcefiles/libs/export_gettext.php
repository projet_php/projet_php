<?php

class ExportGetText
{
  /* Exporte une phrase d’une langue en format GNU gettext. */
  public static function export_gettext ($phrase_à_traduire, $langue)
  {
    $bd = new Database ();
    $retour = "";

    $requête = $bd->prepare ('select id_langue from langues where nom = :langue');
    $requête->execute (array ("langue" => $langue));
    $langue = $requête->fetch ()["id_langue"];

    $requête = 'select id_demande_traduction from traductions_demandees where phrase_a_traduire = :phrase_a_traduire';
    $requête = $bd->prepare ($requête);

    $liaison = array ("phrase_a_traduire" => $phrase_à_traduire);
    if (!$requête->execute ($liaison))
      return false;

    $id_demande_traduction = $requête->fetch (PDO::FETCH_ASSOC)['id_demande_traduction'];
    $retour .= 'msgid "' . $phrase_à_traduire . '"' . "\n";

    $requête = 'select phrase_traduite from traductions where phrase_a_traduire = :id_demande_traduction and langue_destination = :langue';
    $requête = $bd->prepare ($requête);

    $liaison = array ("id_demande_traduction" => $id_demande_traduction,
                      "langue" => $langue);

    if (!$requête->execute ($liaison))
      return false;

    $traduction = $requête->fetch (PDO::FETCH_ASSOC);
    if ($traduction)
      {
        $retour .= 'msgstr "' . $traduction['phrase_traduite'] . "\"\n";
        return $retour;
      }
    else
      return false;
  }
}

?>
