<!DOCTYPE html>
<html>
<head>
	<title>LibreTrad</title>
	<link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
	<meta charset="utf-8">
	<meta name="description" content="Une page qui permet de changer les info des utilisateurs"/>
	<meta name="keywords" content="info,utilisateurs" />
	<link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
</head>
<body>
	<?php 
		Session::init();
		require 'lang.php';
	 ?>
	 	<style type="text/css">
		html
		{
			background-color: #495867;
			background-image: none;
		}
	</style>
	<div id="contact">
		<form method="POST" action="change/update">
			<h2>Changer vos informations personnelles</h2>
				<label id="mail" for="adr-mail">e-mail</label><br/> <input type="email" name="email" value="<?php echo Session::get('mail'); ?>" style="text-align: center;" id="adr-mail"></input><br/><br/>
				<label for="pseudo">Pseudo</label><br/><input type="text" name="pseudo" value="<?php echo Session::get('pseudo'); ?>" style="text-align: center;" id="pseudo"></input><br/><br/>
				<br/>
				<input type="submit" value="Sauvegarder" name="envoyer" id="send">
				<a href="menu_<?php echo Session::get('Compte'); ?>">Annuler</a>
		</form>
	</div>
</body>
</html>
