<html>
  <head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
    <link type="text/css" href="csshake.css">
    <meta name="description" content="page de vérification de compte" />
    <meta name="keywords" content="vérification" />
    <link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
  </head>
  <body>
    <?php
      Session::init();
      require 'lang.php';
      ?>
      <style type="text/css">
  		html
  		{
  			background-color: #495867;
  			background-image: none;
  		}
  	</style>
    <div id="compte_cree">
      <h1>Votre Compte a été activé</h1>
      <p>maintenez connectez-vous ! <a href="connect">ici</a></p>
      <br>
    </div>
  </body>
</html>
