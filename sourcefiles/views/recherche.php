<!DOCTYPE html>
<html>
  <head>
    <title>LibreTrad</title>
    <link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
    <meta name="description" content="page de recherce de traduction" />
	<meta name="keywords" content="standard" />
  </head>
  <body>
    <?php 
      Session::init();
      require 'lang.php';
      ?>
    <div id="recherche">
      <div id="traducteur">
	<form method="POST" action="recherche/recherche">
	  <h2>Langue</h2>
	  <select name="langueDepart">
	    <?php 
	      foreach (Session::get('lang') as $value) 
	      {
	      echo '<option>'.$value['nom'].'</option>';
	      }
	      ?>
	  </select>
	  <p>=></p>
	  <h2></h2>
	  <select name="langueArrivee">
	    <?php
	      foreach (Session::get('lang') as $value) 
	      {
	      echo '<option>'.$value['nom'].'</option>';
	      }
	      ?>
	  </select><br/><br/>
	  <textarea placeholder="Ici votre texte à traduire" name="recherche"></textarea><br/>
	  <input type="submit" value="Traduire" name="translate"><br>
	</form>
      </div>
      <div id="traduction">
	<h2>Traduction : </h2>
	<textarea><?php
		    echo Session::get('traduction');
		    ?></textarea>
      </div>
    </div>
  </body>
</html>
