<!DOCTYPE html>
<html>
<head>
	<title>LibreTrad</title>
	<link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
	<meta charset="utf-8">
	<meta name="description" content="Menu standard" />
	<meta name="keywords" content="standard" />
	<link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
</head>
<body>
	<style type="text/css">
		html
		{
			background-color: #495867;
			background-image: none;
		}
	</style>
	<?php 
		Session::init();
		require 'lang.php';
	 ?>
	 <div id="menu">
		 <div id="profil_standard">
		 	<h1> Votre profil</h1><br/>
		 		<p><strong>Statut</strong></p><pre><?php echo Session::get('Compte'); ?></pre>
		 		<p><strong>Pseudo</strong></p><pre><?php echo Session::get('pseudo'); ?></pre>
		 		<p><strong>mail</strong></p><pre><?php echo Session::get('mail'); ?></pre><br/>
		 		<a href='change'> Modifier </a>
		 </div>
	</div>
</body>
</html>