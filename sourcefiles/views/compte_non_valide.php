<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
    <link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
      <meta name="description" content="page de redirection si le site n'est pas validé" />
  <meta name="keywords" content="validation, redirection" />
  </head>
  <body>
    <?php
      Session::init();
      require 'lang.php';
      ?>
      <style type="text/css">
      html
      {
        background-color: #495867;
        background-image: none;
      }
  </style>
    <div id="compte_cree">
      <h1>Votre compte n'est pas validé !</h1>
      <p>Allez vérifiez vos mails ! <br>
      c'est bon ? <a href="connect">connectez-vous alors !</a></p>
      <br>
    </div>
  </body>
</html>
