<!DOCTYPE html>
<html>
<head>
	<title>LibreTrad</title>
	<link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
	<meta charset="utf-8">
	<link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
	<meta name="description" content="404 not found" />
	<meta name="keywords" content="page introuvable" />
	<script src="sourcefiles/public/js/sketch.js" type="text/javascript"></script>
    <script src="sourcefiles/public/js/star.js" type="text/javascript"></script>
    <script src="sourcefiles/public/js/p5.js" type="text/javascript"></script>
    <script src="sourcefiles/public/js/p5.dom.js" type="text/javascript"></script>
</head>
<body>
	<style type="text/css">
		html,body
		{
			background-image: none;
		}
	</style>
	<?php 
		Session::init();
		require 'lang.php';
	 ?>
	<div id="erreur">
		<div id="equipe"><h1>Page Introuvable dans cet univers !</h1>
			<p>
				Ah je crois que vous êtes perdu ! allez je vous aide, c'est par <a href="index">là ! </a> 
			</p>
		</div>
	</div>
</body>
</html>
