<?php

  Session::init();
  $logged = Session::get('loggedIn');
  if($logged == 1)
    {
      echo '<header>
              <head>
		<link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
              </head>
              <a href="index" id="home"><h1><img src="sourcefiles/public/img/translation.png">LibreTrad</h1></a>
              <div id="trad">
	             <a href= \'recherche\'>' . Traduction::traduire ('Traduire !') . '</a>
              <form method="POST" action="/index/changer_langue" id=\'change_lang\'>
       	        <select name="langue" id="select" onchange="this.form.submit()">';
      foreach (Session::get('lang') as $value)
        {
          if (Session::get ('langue') == $value['nom'])
            echo '<option value="' . $value['nom'] . '" selected>'.$value['nom'].'</option>';
          else if (!Session::get ('langue') && $value['nom'] == "Français")
            echo '<option value="' . $value['nom'] . '" selected>'.$value['nom'].'</option>';
          else
            echo '<option value="' . $value['nom'] . '">'.$value['nom'].'</option>';
        }

      echo '</select>
            </form>
            </div>
            <div id="connect">
            <a href=\'menu_'.Session::get('Compte').'/logout\'>' . Traduction::traduire ('Se déconnecter') . '</a>
            </div>';
    }
  else
    {
     echo '<header>
			<head>
			<link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
			</head>
			<a href="index" id="home"><h1><img src="sourcefiles/public/img/translation.png">LibreTrad</h1></a>
			<div id="trad">
				<a href= \'recherche\'>' . Traduction::traduire ('Traduire !') . '</a>
              <form method="POST" action="/index/changer_langue" id=\'change_lang\'>
       	        <select name="langue" onchange="this.form.submit()">';
      foreach (Session::get('lang') as $value)
        {
          if (Session::get ('langue') == $value['nom'])
            echo '<option value="' . $value['nom'] . '" selected>'.$value['nom'].'</option>';
          else if (!Session::get ('langue') && $value['nom'] == "Français")
            echo '<option value="' . $value['nom'] . '" selected>'.$value['nom'].'</option>';
          else
            echo '<option value="' . $value['nom'] . '">'.$value['nom'].'</option>';
        }

	  echo'</select>
      </form>
			</div>
			<div id="connect">
				<a href=\'connect\'>' . Traduction::traduire ('Se connecter') . '</a>
				<a href=\'insc\'>' . Traduction::traduire ('S’inscrire') . '</a>
			</div>';
    }

?>
