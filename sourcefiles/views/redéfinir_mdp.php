<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
    <link type="text/css" href="csshake.css">
    <meta name="description" content="rédefinition du mot de passe" />
    <meta name="keywords" content="mot de passe" />
    <link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
  </head>
  <body>
    <?php
      Session::init();
      require 'lang.php';
      ?>
      <style type="text/css">
        html
        {
        background-color: #495867;
        background-image: none;
        }
    </style>
    <div id="form-con">
      <form method="POST" action=<?php echo "/mdp_oublie/changer" . $param; ?>>
	<h1>Redéfinir le mot de passe</h1> <br/><br/>
	<label><p>Mot de passe</p><br/><input type="password" name="mdp"></label><br/>
        <label><p>Vérification</p><br/><input type="password" name="vmdp"></label><br/>
	<input type="submit" value="Redéfinir le mot de passe" name="mdp_oublié" id="submit"><br>
      </form>
    </div>
  </body>
</html>
