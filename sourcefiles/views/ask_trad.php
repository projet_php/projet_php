<!DOCTYPE html>
<html>
<head>
	<title>LibreTrad</title>
	<link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
	<meta charset="utf-8">
	<meta name="description" content="Une page qui permet la demande de traduction" />
	<meta name="keywords" content="demande de traduction"/>
	<link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
</head>
<body>
	<?php 
		Session::init();
		require 'lang.php';
	 ?>

	 	<style type="text/css">
		html
		{
			background-color: #495867;
			background-image: none;
		}
	</style>
	<div id="demande">
		<div id="texte_demande">
			<form method="POST" action="ask_trad/ask_for_trad">
				<h2>langue source</h2>
				<select name="langueDepart">
					<?php
					foreach (Session::get('lang') as $value) 
					{
						echo '<option>'.$value['nom'].'</option>';
					}
					?>
				</select>
				</select><br/><br/>
				<textarea placeholder="Ici votre texte à traduire" name="a_traduire"></textarea><br/>
				<input type="submit" value="demander" name="ask"><br>
			</form>
		</div>
	</div>

</body>
</html>
