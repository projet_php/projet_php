  <!DOCTYPE html>
<html>
  <head>
    <title>LibreTrad</title>
    <link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
    <meta charset="utf-8">
    <meta name="description" content="page de mention légales" />
    <meta name="keywords" content="mention légales" />
    <link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
  </head>
  <body>
    <?php
      Session::init();
      require 'lang.php';
      ?>
    <style type="text/css">
      html
      {
      background-color: #495867;
      background-image: none;
      }
    </style>
    <div id="presentation">
      <div id="equipe"><h1>Images Utilisées</h1>
	<p>
	  <div>Icône faite par <a href="https://www.flaticon.com/authors/twitter" title="Twitter">Twitter</a> sur <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> sous licence <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
	</p>
	<p>
	  <div>Icône faite par <a href="http://www.freepik.com" title="Freepik">Freepik</a> sur <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> sous licence <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
	</p>
	<p>
	  <div>Icône faite par <a href="https://www.flaticon.com/authors/eleonor-wang" title="Eleonor Wang">Eleonor Wang</a> sur <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> sous licence <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
	</p>
      </div>
      <div id="droit">
        <nav>
          <a href="#Article-1">1</a>
          <a href="#Article-2">2</a>
          <a href="#Article-3">3</a>
          <a href="#Article-4">4</a>
          <a href="#Article-5">5</a>
        </nav>
        <div id="droit_articles">  
          

          <p id="Article-1"><h1><?php echo Traduction::traduire('Condition général D\'utilisation');?></h1><br><?php echo Traduction::traduire('Les «conditions générales d\'utilisation » ont pour objet l\'encadrement juridique des modalités de mise à disposition des services de libretrad.vendémiaire.fr  et leur utilisation par « l\'Utilisateur ».Ces conditions doivent être acceptées par tout Utilisateur souhaitant accéder au site. Elles constituent le contrat entre le site et l\'Utilisateur. L\’accès au site par l\’Utilisateur signifie son acceptation des présentes conditions générales d\’utilisation.Éventuellement :En cas de non-acceptation des conditions générales d\'utilisation stipulées dans le présent contrat, l\'Utilisateur se doit de renoncer à l\'accès des services proposés par le site.libretrad.vendémiaire.fr se réserve le droit de modifier unilatéralement et à tout moment le contenu des présentes conditions générales d\'utilisation.');?></p>


          <p id="Article-2"><h1><?php echo Traduction::traduire('Hébergeur')?></h1><br><?php echo Traduction::traduire('Ce site est hébergé par Pour contacter cet hébergeur, rendez-vous à l’adresse')?></p>


          <p id="Article-3"><h1><?php echo Traduction::traduire('Définitions')?></h1><br><?php echo Traduction::traduire('La présente clause a pour objet de définir les différents termes essentiels du contrat :Utilisateur : ce terme désigne toute personne qui utilise le site ou l\'un des services proposés par le site.Contenu utilisateur : ce sont les données transmises par l\'Utilisateur au sein du site. 
          Membre : l\'Utilisateur devient membre lorsqu\'il est identifié sur le site. 
          Identifiant et mot de passe : c\'est l\'ensemble des informations nécessaires à l\'identification d\'un Utilisateur sur le site. L\'identifiant et le mot de passe permettent à l\'Utilisateur d\'accéder à des services réservés aux membres du site. Le mot de passe est confidentiel.')?></p>


          <p id="Article-4"><h1><?php echo Traduction::traduire('Accès aux services')?></h1><br><?php echo Traduction::traduire('Le site permet à l\'Utilisateur un accès gratuit aux services suivants :Recherche de Traduction,Demande de traduction,Proposition de traduction.Le site est accessible gratuitement en tout lieu à tout Utilisateur ayant un accès à Internet. Tous les frais supportés par l\'Utilisateur pour accéder au service (matériel informatique, logiciels, connexion Internet, etc.) sont à sa charge.L’Utilisateur non membre n\'a pas accès aux services réservés aux membres. Pour cela, il doit s\'identifier à l\'aide de son identifiant et de son mot de passe')?></p>


          <p id="Article-5"><h1><?php echo Traduction::traduire('Données personnelles')?></h1><br>
          <?php echo Traduction::traduire('Les informations demandées à l’inscription au site sont nécessaires et obligatoires pour la création du compte de l\'Utilisateur. En particulier, l\'adresse électronique pourra être utilisée par le site pour l\'administration, la gestion et l\'animation du service.Le site assure à l\'Utilisateur une collecte et un traitement d\'informations personnelles dans le respect de la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l\'informatique, aux fichiers et aux libertés.En vertu des articles 39 et 40 de la loi en date du 6 janvier 1978, l\'Utilisateur dispose d\'un droit d\'accès, de rectification, de suppression et d\'opposition de ses données personnelles. L\'Utilisateur exerce ce droit via : 
            Son espace personnel.')?>  
         </p>
        </div> 
      </div>
    </div>
  </body>
</html>
