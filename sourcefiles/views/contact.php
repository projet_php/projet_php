<!DOCTYPE html>
<html>
<head>
	<title>LibreTrad</title>
	<link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
	<meta charset="utf-8">
	<meta name="description" content="page de contact" />
	<meta name="keywords" content="contact" />
	<link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
</head>
<body>
	<?php 
		Session::init();
		require 'lang.php';
	 ?>
	 	<style type="text/css">
		html
		{
			background-color: #495867;
			background-image: none;
		}
	</style>
	<div id="contact">
		<h2>Nous contacter</h2>
		<form method="post" action="">
				<label id="mail" for="in-1">e-mail</label><br/><input type="email" name="mail" id="in-1"></input><br/><br/>
				<label class="name" for="in-2">Prénom</label><br/><input type="text" name="prenom" id="in-2"></input><br/><br/>
				<label class="name" for="in-3" >Nom</label><br/><input type="text" name = "nom" id="in-3"></input><br/><br/>
				<label for="in-4"> Votre message</label><br/><textarea placeholder="Ici votre message..." id="in-4"></textarea><br/><br/>
				<input type="submit" value="Envoyer" name="envoyer" id="send"><br>

		</form>
	</div>
</body>
</html>
