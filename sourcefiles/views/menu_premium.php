<!DOCTYPE html>
<html>
<head>
	<title>LibreTrad</title>
	<link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
	<meta charset="utf-8">
	<meta name="description" content="menu premium" />
	<meta name="keywords" content="premium" />
	<link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
</head>
<body>
	<style type="text/css">
		html
		{
			background-color: #495867;
			background-image: none;
		}
	</style>
		<?php
		Session::init();
		require 'lang.php';
	 ?>
	 <div id="menu">
		 <div id="profil">
		 	<h1> Votre profil</h1><br/>
		 		<p><strong>Statut</strong><br><?php echo Session::get('Compte'); ?></p>
		 		<p><strong>Pseudo</strong><br><?php echo Session::get('pseudo'); ?></p>
		 		<p><strong>mail</strong><br><?php echo Session::get('mail'); ?></p><br/>
		 		<a href='change'> Modifier </a>
		 </div>
		 <div id="activity">
		 	<div id="demande_trad">
		 	<h1>Dernières demandes de traduction</h1><br/>
			 				<?php
			 					if(!empty(Session::get('demandes')))
			 					{
			 						foreach(Session::get('demandes') as $value)
			 						{
			 							echo '<p>'.$value['phrase_a_traduire'].'</p>';
			 							echo '<p>'.$value['id_demande_traduction'].'</p>';
			 						}
			 					}
			 					elseif(Session::get('demandes') == NULL)
			 					{
			 						echo 'Aucunes demandes effectuées pour le moment';
			 					}
			 				?>
		 		<br/>
		 	</div>
		 	<a href='ask_trad'> effectuer une demande de traduction</a><br /><br />
                        <form action="/exporter" method="POST">
                          <label for="langue">Langue : </label>
                          <select name="langue">
                          <?php foreach (Session::get('lang') as $value)
                          {
                            if (Session::get ('langue') == $value['nom'])
                              echo '<option value="' . $value['nom'] . '" selected>'.$value['nom'].'</option>';
                            else if (!Session::get ('langue') && $value['nom'] == "Français")
                              echo '<option value="' . $value['nom'] . '" selected>'.$value['nom'].'</option>';
                            else
                              echo '<option value="' . $value['nom'] . '">'.$value['nom'].'</option>';
                          }?>
                          </select>

                          <input name="exporter" type="submit" value="Exporter les traductions vers GetText" />
                        </form>
		 </div>
	</div>
</body>
</html>
