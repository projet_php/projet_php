<!DOCTYPE html>
<html>
<head>
	<title>LibreTrad</title>
	<link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
	<meta charset="utf-8">
	<meta name="description" content="menu administrateur" />
	<meta name="keywords" content="administrateur" />
	<link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
</head>
<body id="corps-admin">
	<style type="text/css">
		html
		{
			background-color: #495867;
			background-image: none;
		}
	</style>
		<?php 
		Session::init();
		require 'lang.php';
	 ?>
	 <div id="menu">
		 <div id="profil">
		 	<h1> Votre profil</h1><br/>
		 		<p><strong>Statut</strong><br><?php echo Session::get('Compte'); ?></p>
		 		<p><strong>Pseudo</strong><br><?php echo Session::get('pseudo'); ?></p>
		 		<p><strong>mail</strong><br><?php echo Session::get('mail'); ?></p><br/>
		 		<a href='change'> Modifier </a>
		 </div>
		 <div id="activity">
		 	<h2>Liste des Utilisateurs</h2>
		 	<div id="demande_trad">
		 		<?php 
		 		foreach (Session::get('liste_utilisateurs') as $value) {
		 			echo '<a id=\'lien_traduire\' href=\'changerUtilisateurs?id='.$value['id_utilisateur'].'&ps='.$value['pseudonyme'].'\'>';
		 			echo '<div id=\'tuple_trad\'>';
		 			echo '<p>'.$value['pseudonyme'].'</p>';
		 			echo '<p>'.$value['adresse_electronique'].'</p>';
		 			echo '<p>'.$value['type_de_compte'].'</p>';
		 			echo '</div>';
		 			echo '</a>';
		 		}
		 		?>
			</div>
			<a href="#activity-2">Gérer les Demandes =></a>
		 </div>
		 <div id="activity-2">
		 	<h2>liste demandes de traduction</h2>
			<div id="demande_trad">
		 		<?php 
		 		foreach (Session::get('demande_traductions') as $value) {
		 			$ph = $value['phrase_a_traduire'];
		 			echo '<a id=\'lien_traduire\' href=\'traduire?id='.$value['id_demande_traduction'].'&ps='.$value['pseudonyme'].'&$ph='.urlencode($ph).'\'>';
		 			echo '<div id=\'tuple_trad\'>';
		 			echo '<p>'.$value['langue_source'].'</p>';
		 			echo '<p>'.$value['phrase_a_traduire'].'</p>';
		 			echo '<p>'.$value['pseudonyme'].'</p>';
		 			echo '</div>';
		 			echo '</a>';
		 		}
		 		?>
			</div>
			<a href="ajouter_langue">Ajouter une langue</a>
			<a href="#corps-admin">Revenir au tableau d'admin</a>	
		 </div>
	</div>
</body>
</html>