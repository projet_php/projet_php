<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
    <link type="text/css" href="csshake.css">
    <meta name="description" content="page de connexion" />
   <meta name="keywords" content="connexion" />
    <link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
  </head>
  <body>
    <?php
      Session::init();
      require 'lang.php';
      ?>
      <style type="text/css">
        html
        {
        background-color: #495867;
        background-image: none;
        }
    </style>
    <div id="form-con">
      <form method="POST" action="connect/verify">
	<h1>Connexion</h1> <br/><br/>
	<label><p>Pseudo</p><br/><input type="text" name="pseudo"></label><br/>
	<label><p>Mot de passe</p><br/><input type="password" name="password"></label><br/>
	<input type="submit" value="Se connecter" name="connect" id="submit"><br>
	<a href="insc">Créer un compte</a>
        <a href="mdp_oublie">Mot de passe oublié</a>
      </form>
    </div>
  </body>
</html>
