<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
    <link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
    <meta name="description" content="page d'Inscription" />
    <meta name="keywords" content="inscription utilisateurs" />
  </head>
  <body>
    <?php
      Session::init();
      require 'lang.php';
      ?>
        <style type="text/css">
    html
    {
      background-color: #495867;
      background-image: none;
    }
  </style>

    <div id="form-insc">
      <form method="POST" action="insc/verify">
	<h1>Inscription</h1> <br/><br/>
	<label><p>Pseudo</p><br/><input type="text" name="pseudo" id="info"></label><br/>
        <label><p>Adresse électronique</p><br/><input type="email" name="adresse_électronique" id="info"></label><br/>
	<label><p>Mot de passe</p><br/><input type="password" name="mot_de_passe" id="info"></label><br/>
	<label><p>Mot de passe de confirmation</p><br/><input type="password" name="vérification_mot_de_passe" id="info"></label><br/>
	<div class="compte">
	  <label><p>Type de compte</p><br/>
	    <label class="radio-label">Standard<input type="radio" name="compte" class="radio" value="standard"></label>
	    <label class="radio-label">Premium<input type="radio" name="compte" class="radio" value="premium"></label>
	    <label class="radio-label">Traducteur<input type="radio" name="compte" class="radio" value="traducteur"></label>
	  </label>
	</div>
    <label class="radio-label">Condition Général d'utilisation<input type="radio" name="conditions" class="radio" value="ok"></label>
	<br/>
	<input type="submit" value="C’est parti !" name="connect" id="envoyer"><br>
      </form>
    </div>
  </body>
</html>
