<!DOCTYPE html>
<html>
<head>
	<title>LibreTrad</title>
	<link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
	<meta charset="utf-8">
	<meta name="description" content="Une page qui permet l'ajout d'une langue au système" />
	<meta name="keywords" content="langue, sytème de traduction" />
	<link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
</head>
<body>
	<?php 
		Session::init();
		require 'lang.php';
	 ?>
	 <div id="menu">
		 <div id="profil_standard">
		 	<h1>Ajouter une langue</h1><br/>
		 		<form method="POST" action="ajouter_langue/ajouter_langue">
		 			<label for="nm_lg">Nom de la langue</label><br>
		 			<input type="text" name="nom_langue" id="nm_lg"><br>
		 			<input type="submit" name="go"></input>
		 		</form>
		 </div>
	</div>
</body>
</html>