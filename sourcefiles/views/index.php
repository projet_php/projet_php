<!DOCTYPE html>
<html>
  <head>
    <title>LibreTrad</title>
    <link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
    <meta charset="utf-8">
    <meta name="description" content="page d'index" />
    <meta name="keywords" content="présentation du site" />
    <link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
  </head>
  <body>
    <?php
      Session::init();
      require 'lang.php';
      ?>
    <div id="presentation">
      <div id="equipe"><h1><?php echo Traduction::traduire ('Équipe'); ?></h1>
	<p>
	  <?php echo Traduction::traduire ("Une fière équipe de développeurs composée exclusivement d’étudiants du DUT informatique d’Aix-en-provence.Cette équipe est donc composé de Corentin Bocquillon, Eliott Bence, Rémi Guijarro et Nicolas Brault .Elle a été crée en l’an de grâce 2017 afin de pouvoir répondre à la forte demande de création de site de traduction qui ne cesse de croître !Nous vous souhaitons une très bonne navigation à travers notre site et n’hésitez pas à nous contacter en cas de demande ou de problème avec l’onglet « Nous contacter ».");?>
	</p>
      </div>
      <div id="fonctionnement"><h1><?php echo Traduction::traduire ('Fonctionnement'); ?></h1><p><?php echo Traduction::traduire ("Il vous suffit de vous rendre dans l’onglet « Traduire ! » pour commencer à traduire vos textes… Vous n’êtes pas satisfait ? Vous voulez traduire sans devoir attendre ? Dans ce cas inscrivez-vous sans attendre et profitez de traductions illimitées ! Cependant si vous voulez le meilleur du meilleur nous vous conseillons le compte premium qui vous permettra d’obtenir des réponses dans le cas ou la traduction qui vous intéresse n’existe pas encore ! Enfin si vous aimez aidez les autres, en apprendre plus tous les jours et posséder les avantages du premium c’est le compte traducteur qu’il vous faut !"); ?>
      </p></div>
    </div>
  </body>
</html>
