<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
    <link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
    <meta name="description" content="validation de compte" />
  <meta name="keywords" content="Compte crée" />
  </head>
  <body>
    <?php
      Session::init();
      require 'lang.php';
      ?>
      <style type="text/css">
        html
        {
          background-color: #495867;
          background-image: none;
        }
      </style>
    <div id="compte_cree">
      <h1>Bravo et bienvenue à bord !</h1>
      <p>maintenez connectez-vous ! <a href="connect">ici</a></p>
      <br>
    </div>
  </body>
</html>
