<!DOCTYPE html>
<html>
<head>
	<title>LibreTrad</title>
	<link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
	<meta charset="utf-8">
	<meta name="description" content="Une page qui permet de changer le type de compte d'un utilisateurs" />
	<meta name="keywords" content="compte, utilisateurs"/>
	<link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
</head>
<body>
	<?php 
		Session::init();
		require 'lang.php';
	 ?>
	 <div id="menu">
		 <div id="profil_standard">
		 	<h1> Modification de <?php echo Session::get('pseudo_demande'); ?></h1><br/>
		 		<form method="POST" action="changerUtilisateurs/update_utilisateurs">
		 			<select name="type_compte">
		 				<option>standard</option>
		 				<option>premium</option>
		 				<option>traducteur</option>
		 				<option>admin</option>
		 			</select>
		 			<input type="submit" name="go"></input>
		 		</form>
		 </div>
	</div>
</body>
</html>