<!DOCTYPE html>
<html>
<head>
	<title>LibreTrad</title>
	<link rel="stylesheet" type="text/css" href="sourcefiles/public/css/index.css">
	<meta charset="utf-8">
	<meta name="description" content="page de traduction pour les traducteur" />
	<meta name="keywords" content="traducteur" />
	<link rel="icon" type="image/png" href="sourcefiles/public/img/translate.png" />
</head>
<body>
	<?php 
		Session::init();
		require 'lang.php';
	 ?>
	 <div id="menu">
		 <div id="profil_standard">
		 	<h1> Traduction demandée par  <?php echo Session::get('pseudo_demande'); ?></h1><br/>
		 		<form method="POST" action="traduire/insert_trad">
		 			<h3>Phrase à traduire</h3>
		 			<p id='text_a_traduire'><?php echo Session::get('phrase_a_traduire'); ?></p>
		 			<h6>Choisissez la langue de traduction</h6>
		 			<select name="langue">
		 				<?php

		 				    foreach (Session::get('lang') as $value)
        					{
          						echo '<option>'.$value['nom'].'</option>';
        					}
		 				?>
		 			</select>
		 			<textarea name="traduction"></textarea><br>
		 			<input type="submit" name="go"></input>
		 		</form>
		 </div>
	</div>
</body>
</html>