<?php
class menu_standard extends Controller
{
  function __construct()
  {
    parent::__construct();
    Session::init();
  }

  /* Affichage de l’espace utilisateur standard. */
  function index()
  {
    $logged = Session::get('loggedIn');
    if($logged == false)
      {
        Session::destroy();
        header("location: connect");
        exit;
      }
    else
      {
        $this->view->render('menu_'.Session::get('Compte'));
      }
  }

  /* Déconnexion. */
  function logout()
  {
    Session::destroy();
    header("location: ../connect");
    exit;
  }
}
?>
