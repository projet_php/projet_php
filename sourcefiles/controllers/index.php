<?php
class Index extends Controller
{
  function __construct()
  {
    parent::__construct();
    Session::init();
  }

  function index()
  {
    $this->view->render('index');
  }

  /* Permet de changer la langue d’affichage du site. */
  function changer_langue ()
  {
    Session::init ();
    Session::set ('langue', $_POST['langue']);
    header('Location: /');
  }
}
?>
