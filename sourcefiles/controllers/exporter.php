<?php
class Exporter extends Controller
{
  function __construct()
  {
    parent::__construct();
  }

  /* Affiche le fichier contenant les traductions au format de GNU
     Gettext. */
  function index()
  {
    $logged = Session::get('loggedIn');
    if($logged == false)
      {
        header("location: connect");
        exit;
      }
    else
      {
        header('Content-Type: text/plain');
        header('Content-Disposition: attachment');

        $langue = filter_input (INPUT_POST, 'langue');
        $demandes = $this->model->obtenir_demandes (Session::get ('id_util'));
        foreach ($demandes as $demande)
          {
            $export = ExportGetText::export_gettext ($demande[0], $langue);
            if ($export)
              echo $export. "\n";
          }
      }
  }
}
?>
