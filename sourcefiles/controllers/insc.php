<?php

class insc extends Controller
{
  function __construct()
  {
    parent::__construct();
    Session::init();
  }

  function index()
  {
    $this->view->render('insc');
  }

  /* Inscription dans la base de données de l’utilisateur. */
  function verify()
  {
    $pseudo = filter_input (INPUT_POST, "pseudo");
    $mot_de_passe = filter_input (INPUT_POST, "mot_de_passe");
    $vérification_mot_de_passe = filter_input (INPUT_POST,
                                               "vérification_mot_de_passe");
    $adresse_électronique = filter_input (INPUT_POST, "adresse_électronique",
                                          FILTER_VALIDATE_EMAIL);
    if (!$adresse_électronique)
      unset ($adresse_électronique);

    $compte = filter_input (INPUT_POST, "compte");

    if (!isset ($pseudo) || !isset ($mot_de_passe)
        || !isset ($vérification_mot_de_passe)
        || !isset ($compte) || !isset ($adresse_électronique)
        || $mot_de_passe != $vérification_mot_de_passe)
      {
        header ("Location : insc");
      }
    else
    {
      //Générer clé aléatoire
       $clé = md5(microtime(TRUE)*100000);
       if($this->model->verify($pseudo,$mot_de_passe,$adresse_électronique,$compte,$clé))
       {
         //Préparation du mail
         $to  = $_POST['adresse_électronique'];

         // Sujet
         $subject = 'LibreTrad vérification d’adresse électronique';

         // message
         $message = '
Vérifiez votre adresse électronique pour vous connecter ! :)

Veuillez cliquez sur le lien ci-dessous pour confirmez votre adresse électronique
http://libretrad.xn--vendmiaire-e7a.fr/verification/verify/?log='.urlencode($pseudo).'&cle='.urlencode($clé).'

-------------------------------------------------------------------
Ceci est un courriel automatique, veuillez ne pas répondre.';

         mail ($to, $subject, $message);

         header("location: ../compte_cree");
       }
    }
  }
}

?>
