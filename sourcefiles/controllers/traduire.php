		<?php
	class Traduire extends Controller
	{
		function __construct()
		{
			parent::__construct();
			Session::init();
		}

		function index()
		{
			$logged = Session::get('loggedIn');
			if($logged == false) 
			{
				Session::destroy(); 
				header("location: connect");
				exit;
			}
			else
			{
				$id = filter_input(INPUT_GET,'id');
				$pseudo = filter_input(INPUT_GET, 'ps');
				$phrase = filter_input(INPUT_GET, '$ph');
				Session::set('id_article',$id);
				Session::set('pseudo_demande',$pseudo);
				Session::set('phrase_a_traduire',$phrase);
				$this->view->render('traduire');
			}
		}

		function insert_trad()
		{
			$langue = filter_input(INPUT_POST, 'langue');
			$traduction = filter_input(INPUT_POST, 'traduction');

			if($this->model->insert_trad($langue,$traduction))
			{
				header('location: ../menu_traducteur');
			}
		}
	}
?>