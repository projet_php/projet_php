<?php
class Change extends Controller
{
  function __construct()
  {
    parent::__construct();
    Session::init();
  }

  /* Affiche la page de changement. */
  function index()
  {
    $logged = Session::get('loggedIn');
    if($logged == false)
      {
        Session::destroy();
        header("location: connect");
        exit;
      }
    else
      {
        $this->view->render('change');
      }
  }

  /* Change les informations de l’utilisateur. */
  function update()
  {
    $logged = Session::get('loggedIn');
    if($logged == false)
      {
        Session::destroy();
        header("location: ../connect");
        exit;
      }
    else
      {
        $pseudo = filter_input(INPUT_POST, 'pseudo');
        $mail = filter_input(INPUT_POST, 'email');
        if($this->model->update($pseudo,$mail))
          {
            header('Location: ../menu_'.Session::get('Compte'));
          }
        else
          {
            echo "pas";
          }
      }
  }
}
?>
