<?php
class Mentions extends Controller
{
  function __construct()
  {
    parent::__construct();
    Session::init();
  }

  /* Affiche la vue des mentions légales. */
  function index()
  {
    $this->view->render('mentions');
  }
}
?>
