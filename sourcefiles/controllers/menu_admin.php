<?php
class Menu_Admin extends Controller
{
  function __construct()
  {
    parent::__construct();
    Session::init();
  }

  /* Affiche le menu d’administration. */
  function index()
  {
    /* Il faut être connecté. */
    $logged = Session::get('loggedIn');
    if($logged == false)
      {
        Session::destroy();
        header("location: connect");
        exit;
      }
    else
      {
        if($this->model->afficher_Utilisateurs() && $this->model->afficher_Demandes())
          {
            Session::set('liste_utilisateurs',$this->model->afficher_Utilisateurs());
            $data = $this->model->afficher_Demandes();
            Session::set('demande_traductions',$data);
          }
        $this->view->render('menu_'.Session::get('Compte'));
      }
  }

  /* Déconnexion. */
  function logout()
  {
    Session::destroy();
    header("location: ../connect");
    exit;
  }
}
?>