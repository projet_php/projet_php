<?php
class menu_traducteur extends Controller
{
  function __construct()
  {
    parent::__construct();
    Session::init();
  }

  /* Affichage de l’espace traducteur. */
  function index()
  {
    $logged = Session::get('loggedIn');
    if($logged == false)
      {
        Session::destroy();
        header("location: connect");
        exit;
      }
    else
      {
        if($this->model->afficher_Demandes())
          {
            $data = $this->model->afficher_Demandes();
            Session::set('demande_traductions',$data);
          }
        $this->view->render('menu_'.Session::get('Compte'));
      }
  }

  /* Déconnexion. */
  function logout()
  {
    Session::destroy();
    header("location: ../connect");
    exit;
  }
}
?>
