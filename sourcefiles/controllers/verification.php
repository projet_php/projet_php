<?php
class Verification extends Controller
{
  function __construct()
  {
    parent::__construct();
    Session::init();
  }

  function index()
  {
    $this->view->render('verification');
  }

  /* Vérification de la clef et du compte. */
  function verify()
  {
    // Récupération des variables
    $login = filter_input(INPUT_GET,'log');
    $cle = filter_input(INPUT_GET, 'cle');

    if($this->model->verify($login,$cle))
      {
        $row = $this->model->verify($login);
        $clebdd = $row['cle'];
        $verifie = $row['verifie'];
      }

      if($verifie == '1')
      {
        echo "Votre compte est déjà actif !";
      }

      else
      {
        if($cle == $clebdd)
          {
            if($this->model->update($login, md5(microtime(TRUE)*100000)))
            {
              header("location: ../../verification");
            }
          }
          else
          {
            echo "Erreur ! Votre compte ne peut être activé...";
          }
      }

    }
  }
    ?>
