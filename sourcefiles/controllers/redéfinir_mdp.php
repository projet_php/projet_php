<?php
class redéfinir_mdp extends Controller
{
  function __construct ()
  {
    parent::__construct ();
    Session::init ();
  }

  /* Affichage de la vue contenant un formulaire de changement de mot
     de passe. */
  function index ()
  {
    $pseudo = filter_input (INPUT_GET, 'pseudo');
    $clef = filter_input (INPUT_GET, 'clef');
    $this->view->render('redéfinir_mdp', '?pseudo='.urlencode($pseudo).'&clef='.urlencode($clef));
  }
}
?>
