<?php
class Compte_non_valide extends Controller
{
  function __construct()
  {
    parent::__construct();
    Session::init();
  }

  /* Affichage de la vue compte non validé. */
  function index()
  {
    $this->view->render('compte_non_valide');
  }
}
?>
