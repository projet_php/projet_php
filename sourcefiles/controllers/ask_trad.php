<?php
class Ask_Trad extends Controller
{
  function __construct()
  {
    parent::__construct();
    Session::init();
  }

  function index()
  {
    $logged = Session::get('loggedIn');
    if($logged == false)
      {
        Session::destroy();
        header("location: connect");
        exit;
      }
    else
      {
        $this->view->render('ask_trad');
      }
  }

  /* Insère une demande de traduction dans la base de données. */
  function ask_for_trad()
  {
    $langueDep = filter_input(INPUT_POST, 'langueDepart');
    $traductionDemandee = filter_input(INPUT_POST, 'a_traduire');

    if($this->model->ask_for_trad($langueDep,$traductionDemandee))
      {
        header('location: ../ask_trad');
      }
  }
}
?>
