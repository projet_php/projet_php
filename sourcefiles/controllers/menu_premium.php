<?php
class menu_premium extends Controller
{
  function __construct()
  {
    parent::__construct();
    Session::init();
  }

  /* Affiche l’espace premium. */
  function index()
  {
    $logged = Session::get('loggedIn');
    if($logged == false)
      {
        Session::destroy();
        header("location: connect");
        exit;
      }
    else
      {
        $id = Session::get('id_util');
        if($this->model->afficher_demandes($id))
          {
            Session::set('demandes',$this->model->afficher_demandes($id));
            $this->view->render('menu_'.Session::get('Compte'));
          }
        elseif(Session::set('demandes',$this->model->afficher_demandes($id)) == NULL)
          {
            $this->view->render('menu_'.Session::get('Compte'));
          }
      }
  }

  /* Déconnexion. */
  function logout()
  {
    Session::destroy();
    header("location: ../connect");
    exit;
  }
}
?>
