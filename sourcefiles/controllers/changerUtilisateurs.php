<?php
class ChangerUtilisateurs extends Controller
{
  function __construct()
  {
    parent::__construct();
    Session::init();
  }
  /* Affiche la vue permettant de changer le type de compte d’un
     utilisateur. */
  function index()
  {
    $logged = Session::get('loggedIn');
    if($logged == false)
      {
        Session::destroy();
        header("location: connect");
        exit;
      }
    else
      {
        $id = filter_input(INPUT_GET,'id');
        $pseudo = filter_input(INPUT_GET, 'ps');
        Session::set('id_article',$id);
        Session::set('pseudo_demande',$pseudo);
        $this->view->render('changerUtilisateurs');
      }
  }

  /* Change le type de compte de l’utilisateur. */
  function update_utilisateurs()
  {
    $compte = filter_input(INPUT_POST, 'type_compte');
    $pseudo = Session::get('pseudo_demande');

    if($this->model->update_utilisateurs($compte,$pseudo))
      {
        header('location: ../menu_admin');
      }
  }
}
?>
