<?php
class Mdp_Oublie extends Controller
{
  function __construct ()
  {
    parent::__construct ();
    Session::init ();
  }

  function index ()
  {
    $this->view->render('mdp_oublie');
  }

  /* Vérification de la clef et affichage de la vue permettant de
     redéfinir le mot de passe. */
  function vérifier ()
  {
    $pseudo = filter_input (INPUT_GET, 'pseudo');
    $clef = filter_input (INPUT_GET, 'clef');

    if ($this->model->vérifier ($pseudo, $clef))
      header ('Location: /redéfinir_mdp?pseudo='.urlencode($pseudo).'&clef='.urlencode($clef));
    else
      header ("Location: /");
  }

  /* Changement du mot de passe. */
  function changer ()
  {
    /* On récupère les paramètres. */
    $pseudo = filter_input (INPUT_GET, 'pseudo');
    $clef = filter_input (INPUT_GET, 'clef');
    $mdp = filter_input (INPUT_POST, 'mdp');
    $vmdp = filter_input (INPUT_POST, 'vmdp');

    /* Si le champ de vérification n’a pas la même valeur que le champ
       mot de passe on redirige vers l’accueil. */
    if ($mdp != $vmdp || !$this->model->vérifier ($pseudo, $clef))
      header ("Location: /");
    else                        /* On change le mot de passe. */
      {
        if ($this->model->changer ($pseudo, $mdp))
          header ("Location: /connect");
        else
          header ("Location: /");
      }
  }

  /* On envoit la demande de changement de mot de passe par
     courriel. */
  function envoyer ()
  {
    $pseudo = filter_input (INPUT_POST, 'pseudo');
    $adresse = $this->model->obtenir_adresse ($pseudo);
    if (!$adresse)
      {
        header ("Location: /");
        die ();
      }

    $clef = md5 (microtime (TRUE) * 100000);
    $this->model->définir_clef ($pseudo, $clef);

    // Sujet
    $sujet = 'LibreTrad mot de passe oublié';

    // message
    $message = '
Suivez le lien suivant pour réinitialiser votre mot de passe.

http://libretrad.xn--vendmiaire-e7a.fr/mdp_oublie/vérifier?pseudo='.urlencode($pseudo).'&clef='.urlencode($clef).'

-------------------------------------------------------------------
Ceci est un mail automatique, veuillez ne pas répondre à celui-ci';

    mail ($adresse, $sujet, $message);
    header ("Location: /connect");
  }
}
?>
