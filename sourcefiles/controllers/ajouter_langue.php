<?php
class Ajouter_langue extends Controller
{
  function __construct()
  {
    parent::__construct();
    Session::init();
  }

  function index()
  {
    $logged = Session::get('loggedIn');
    if($logged == false)
      {
        Session::destroy();
        header("location: connect");
        exit;
      }
    else
      {
        $this->view->render('ajouter_langue');
      }
  }

  /* Permet d’ajouter une langue dans la base de données. */
  function ajouter_langue()
  {
    $langue = filter_input(INPUT_POST, 'nom_langue');

    if($this->model->ajouter_langue($langue))
      {
        header('location: ../menu_admin');
      }
  }
}
?>