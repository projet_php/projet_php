<?php

class Dashboard extends Controller
{
  function __construct()
  {
    parent::__construct();
    Session::init();
  }

  function index()
  {
    $logged = Session::get('loggedIn');
    if($logged == false)
      {
        Session::destroy();
        header("location: connect");
        exit;
      }
    else
      {
        $this->view->render('dashboard');
      }
  }

  /* Déconnecte l’utilisateur. */
  function logout()
  {
    Session::destroy();
    header("location: connect");
    exit;
  }
}

?>
