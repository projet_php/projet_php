<?php
class Connect extends Controller
{
  function __construct()
  {
    parent::__construct();
    Session::init();
  }

  function index()
  {
    $this->view->render('connect');
  }

  /* Vérifie le mot de passe. */
  function verify()
  {
    $this->model->verify();
  }

}
?>
