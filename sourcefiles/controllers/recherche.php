<?php
class Recherche extends Controller
{
  function __construct()
  {
    parent::__construct();
    Session::init();
  }

  function index()
  {
    $this->view->render('recherche');
    Session::set ('traduction', '');
  }

  /* Affiche les traductions. */
  function recherche()
  {
    $recherche =  filter_input(INPUT_POST,"recherche");
    $langueDepart = filter_input(INPUT_POST,"langueDepart");
    $langueArrivee = filter_input(INPUT_POST,"langueArrivee");
    $traduction = $this->model->recherche($recherche,$langueDepart,$langueArrivee);
    if($traduction)
    {
      if(!isset($_SESSION['loggedIn']))
      {
        $tampax =  time() - Session::get('last_traduction');
        if($tampax > 600)
        {
          Session::init();
          Session::set('traduction', $traduction);
          $dateRecherche = time();
          Session::set('last_traduction',$dateRecherche);
          header("location: ../../recherche");
        }
        else
        {
             $traduction = 'Vous devez attendre 10min ou vous connecter';
            Session::set('traduction', $traduction);
             header("location: ../../recherche");
        }
      }
      else
      {
        Session::init();
        Session::set('traduction', $traduction);
        $dateRecherche = time();
        Session::set('last_traduction',$dateRecherche);
        header("location: ../../recherche");
      }
    }
    else
    {
      $traduction = 'Cet phrase ne peut être traduite ! vous devez être premium pour faire un demande de traduction';
      Session::set('traduction', $traduction);
      header("location: ../../recherche");
    }
  }
}
?>
